//
//  TaxiSearchAPIClientProtocol.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "APIMappingModel.h"
#import "Taxi.h"

@protocol TaxiSearchAPIClientProtocol <NSObject>
- (void)getTaxisBetweenCoordinate1:(CLLocationCoordinate2D)coordinate1 coordinate2:(CLLocationCoordinate2D)coordinate2 completion:(void(^)(NSArray <TaxiModel *>* response, NSError *error))completion;
@end
