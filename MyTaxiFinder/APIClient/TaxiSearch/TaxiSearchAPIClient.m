//
//  TaxiSearchAPIClient.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "TaxiSearchAPIClient.h"
#import "APIMappingModel.h"
#import "AFNetworking.h"

@interface TaxiSearchAPIClient ()
@property (nonatomic, strong) AFHTTPRequestOperationManager *operationManager;
@end

@implementation TaxiSearchAPIClient

- (id)init {
    self = [super init];
    if (self) {
        //TODO: Store baseUrl at PList file
        NSURL *baseUrl = [NSURL URLWithString:@"https://poi-api.mytaxi.com"];
        self.operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseUrl];
        self.operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

- (void)getTaxisBetweenCoordinate1:(CLLocationCoordinate2D)coordinate1 coordinate2:(CLLocationCoordinate2D)coordinate2 completion:(void(^)(NSArray <TaxiModel *>* response, NSError *error))completion {
    if (!completion) {
        return;
    }

    NSString *endPoint = @"/PoiService/poi/v1";
    NSDictionary *parameters = @{
                                 @"p1Lat" : @(coordinate1.latitude),
                                 @"p1Lon" : @(coordinate1.longitude),
                                 @"p2Lat" : @(coordinate2.latitude),
                                 @"p2Lon" : @(coordinate2.longitude)
                                 };

    [self.operationManager GET:endPoint parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (operation.response.statusCode == 200) {
            NSError *error;
            TaxisModel *taxisModel = [MTLJSONAdapter modelOfClass:TaxisModel.class fromJSONDictionary:responseObject error:&error];

            completion(taxisModel.taxis, error);
        } else {
            completion(nil, operation.error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(nil, error);
    }];
}
@end
