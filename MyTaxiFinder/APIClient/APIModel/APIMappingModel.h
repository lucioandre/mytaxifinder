//
//  APIMappingModel.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"
#import "Taxi.h"

@interface TaxiModel : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy, readonly) NSNumber *identifier;
@property (nonatomic, copy, readonly) NSNumber *latitude;
@property (nonatomic, copy, readonly) NSNumber *longitude;
@property (nonatomic, copy, readonly) NSString *state;
@property (nonatomic, copy, readonly) NSString *type;
@property (nonatomic, copy, readonly) NSNumber *heading;
@end

@interface TaxisModel : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy, readonly) NSArray<TaxiModel *> *taxis;
@end
