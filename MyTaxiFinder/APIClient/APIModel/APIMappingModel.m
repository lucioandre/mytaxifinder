//
//  APIMappingModel.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "APIMappingModel.h"

@implementation TaxiModel
+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[NSDictionary mtl_identityPropertyMapWithModel:self] mutableCopy];
    mapping[@"identifier"] = @"id";
    mapping[@"latitude"] = @"coordinate.latitude";
    mapping[@"longitude"] = @"coordinate.longitude";
    return mapping;
}
@end

@implementation TaxisModel
+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[NSDictionary mtl_identityPropertyMapWithModel:self] mutableCopy];
    mapping[@"taxis"] = @"poiList";
    return mapping;
}

+ (NSValueTransformer *)taxisJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:TaxiModel.class];
}

@end
