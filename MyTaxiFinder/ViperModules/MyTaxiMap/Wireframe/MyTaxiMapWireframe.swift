//
// Created by Lucio
// Copyright (c) 2017 Company. All rights reserved.
//

import Foundation
import UIKit

@objc class MyTaxiMapWireframe: NSObject, MyTaxiMapWireframeProtocol {

    class func configureViewController() -> UIViewController {
        // Generating module components
        let view: MyTaxiMapViewProtocol = MyTaxiMapView()
        let presenter: MyTaxiMapPresenterProtocol = MyTaxiMapPresenter()
        let interactor: MyTaxiMapInteractorProtocol = MyTaxiMapInteractor(taxiSearchAPIClient: TaxiSearchAPIClient())
        let wireFrame: MyTaxiMapWireframeProtocol = MyTaxiMapWireframe()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor

        return view as! UIViewController
    }
}
