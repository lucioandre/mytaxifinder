//
// Created by Lucio
// Copyright (c) 2017 Company. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MyTaxiMapView: UIViewController, MyTaxiMapViewProtocol {
    var presenter: MyTaxiMapPresenterProtocol?

    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapView.delegate = self
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "Map"
        self.configureInitialMapState()
    }

    func configureInitialMapState() {
        let coordinate1 = CLLocationCoordinate2DMake(53.694865, 9.757589)
        let coordinate2 = CLLocationCoordinate2DMake(53.394655, 10.099891)
        let p1 = MKMapPointForCoordinate(coordinate1);
        let p2 = MKMapPointForCoordinate(coordinate2);
        let mapRect = MKMapRectMake(fmin(p1.x,p2.x), fmin(p1.y,p2.y), fabs(p1.x-p2.x), fabs(p1.y-p2.y));
        self.mapView.setVisibleMapRect(mapRect, animated: false)
    }

    //MARK: Protocol

    func updateUIWithSearchResults(taxis:[Taxi]) {
        var taxiAnnotations = [MKAnnotation]()
        for taxi in taxis {

            let locationCoordinate = CLLocationCoordinate2DMake(taxi.latitude.doubleValue, taxi.longitude.doubleValue)

            let annotation = CarAnnotation(coordinate: locationCoordinate, headingAngle: taxi.heading.doubleValue)
            taxiAnnotations.append(annotation)
        }
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.addAnnotations(taxiAnnotations)
    }
}

extension MyTaxiMapView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.presenter?.searchRegionDidChange(topLeftCoordinate: mapView.topLeftCoordinate(), bottomRightCoordinate: mapView.bottomRightCoordinate())
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let carAnnotation = annotation as? CarAnnotation {
            let reuseIdentifier = "CarAnnotationView"
            var carAnnotationView:CarAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? CarAnnotationView

            if carAnnotationView != nil {
                carAnnotationView!.annotation = annotation
            } else {
                carAnnotationView = CarAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            }

            carAnnotationView?.transform = CGAffineTransform(rotationAngle: CGFloat(carAnnotation.headingAngle) * CGFloat.pi / 180.0)
            return carAnnotationView
        }
        else {
            return nil
        }
    }
}

extension MKMapView {
    func topLeftCoordinate() -> CLLocationCoordinate2D {
        return convert(CGPoint.zero, toCoordinateFrom: self)
    }

    func bottomRightCoordinate() -> CLLocationCoordinate2D {
        return convert(CGPoint(x: frame.width, y: frame.height), toCoordinateFrom: self)
    }
}
