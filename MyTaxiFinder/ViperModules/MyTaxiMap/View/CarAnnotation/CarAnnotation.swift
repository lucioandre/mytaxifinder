//
//  CarAnnotation.swift
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 15/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

import UIKit
import MapKit

class CarAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var headingAngle:Double

    init(coordinate:CLLocationCoordinate2D, headingAngle:Double) {
        self.coordinate = coordinate
        self.headingAngle = headingAngle
    }
}
