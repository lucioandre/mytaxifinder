//
//  CarAnnotationView.swift
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 15/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

import UIKit
import MapKit

class CarAnnotationView: MKAnnotationView {

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.image = UIImage(named: "taxi-thumb")
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
