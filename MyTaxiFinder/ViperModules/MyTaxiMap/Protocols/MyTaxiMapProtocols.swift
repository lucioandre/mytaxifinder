//
// Created by Lucio
// Copyright (c) 2017 Company. All rights reserved.
//

import Foundation
import UIKit

protocol MyTaxiMapViewProtocol: class {
    var presenter: MyTaxiMapPresenterProtocol? { get set }
    func updateUIWithSearchResults(taxis:[Taxi])
}

protocol MyTaxiMapWireframeProtocol: class {}

protocol MyTaxiMapPresenterProtocol: class {
    var view: MyTaxiMapViewProtocol? { get set }
    var interactor: MyTaxiMapInteractorProtocol? { get set }
    var wireFrame: MyTaxiMapWireframeProtocol? { get set }

    func searchRegionDidChange(topLeftCoordinate:CLLocationCoordinate2D, bottomRightCoordinate:CLLocationCoordinate2D)
}

protocol MyTaxiMapInteractorProtocol: MyTaxiListInteractorProtocol {

}
