//
// Created by Lucio
// Copyright (c) 2017 Company. All rights reserved.
//

import Foundation

class MyTaxiMapPresenter: MyTaxiMapPresenterProtocol {
    weak var view: MyTaxiMapViewProtocol?
    var interactor: MyTaxiMapInteractorProtocol?
    var wireFrame: MyTaxiMapWireframeProtocol?
    
    init() {}

    func searchRegionDidChange(topLeftCoordinate:CLLocationCoordinate2D, bottomRightCoordinate:CLLocationCoordinate2D) {
        self.interactor?.searchTaxisBetweenCoordinate1(topLeftCoordinate, coordinate2: bottomRightCoordinate, onCompletion: { (taxis:[Taxi]?, error) in
            if error == nil, let _taxis = taxis {
                self.view?.updateUIWithSearchResults(taxis: _taxis)
            }
        })
    }
}
