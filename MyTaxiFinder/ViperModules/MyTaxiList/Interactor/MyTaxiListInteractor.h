//
//  MyTaxiListInteractor.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTaxiListProtocols.h"
#import "TaxiSearchAPIClientProtocol.h"

@interface MyTaxiListInteractor : NSObject<MyTaxiListInteractorProtocol>
- (instancetype)initWithTaxiSearchAPIClient:(id<TaxiSearchAPIClientProtocol>)taxiSearchAPIClient;
@end
