//
//  MyTaxiListInteractor.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "MyTaxiListInteractor.h"


@interface MyTaxiListInteractor ()
@property (nonatomic) id<TaxiSearchAPIClientProtocol> taxiSearchAPIClient;
@end

@implementation MyTaxiListInteractor
- (instancetype)initWithTaxiSearchAPIClient:(id<TaxiSearchAPIClientProtocol>)taxiSearchAPIClient {
    if (self = [super init]) {
        self.taxiSearchAPIClient = taxiSearchAPIClient;
    }
    return self;
}

#pragma mark - Protocol

- (void)searchTaxisBetweenCoordinate1:(CLLocationCoordinate2D)coordinate1 coordinate2:(CLLocationCoordinate2D)coordinate2 onCompletion:(void(^)(NSArray <Taxi*> *results, NSError *error))completion {

    [self.taxiSearchAPIClient getTaxisBetweenCoordinate1:coordinate1 coordinate2:coordinate2 completion:^(NSArray<TaxiModel *> *response, NSError *error) {

        completion([self extractTaxiPlainObjectsFromTaxiModels:response], error);
    }];
}

#pragma mark - Taxi Plain Object Extractor

- (NSArray<Taxi *> *)extractTaxiPlainObjectsFromTaxiModels:(NSArray<TaxiModel*> *)taxis {
    NSMutableArray<Taxi *> *plainTaxis = [NSMutableArray new];
    for (TaxiModel *taxiModel in taxis) {
        Taxi *plainTaxi = [[Taxi alloc] init];
        plainTaxi.identifier = taxiModel.identifier;
        plainTaxi.latitude = taxiModel.latitude;
        plainTaxi.longitude = taxiModel.longitude;
        plainTaxi.heading = taxiModel.heading;
        plainTaxi.state = taxiModel.state;
        plainTaxi.type = taxiModel.type;
        [plainTaxis addObject:plainTaxi];
    }
    return plainTaxis;
}

@end
