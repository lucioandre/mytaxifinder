//
//  Taxi.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Taxi : NSObject
@property (nonatomic, copy) NSNumber *identifier;
@property (nonatomic, copy) NSNumber *latitude;
@property (nonatomic, copy) NSNumber *longitude;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSNumber *heading;
@end
