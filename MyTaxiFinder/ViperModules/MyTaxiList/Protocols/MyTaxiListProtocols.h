//
//  MyTaxiListProtocols.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Taxi.h"
#import <CoreLocation/CoreLocation.h>

@protocol MyTaxiListViewProtocol <NSObject>
- (void)showProgressIndicator;
- (void)hideProgressIndicator;
- (void)updateTaxiSearchResults:(NSArray<Taxi *> *)results;
@end

@protocol MyTaxiListPresenterProtocol <NSObject>
- (void)viewDidLoadEvent;
- (void)reloadDataEvent;
@end

@protocol MyTaxiListWireframeProtocol <NSObject>

@end

@protocol MyTaxiListInteractorProtocol <NSObject>
- (void)searchTaxisBetweenCoordinate1:(CLLocationCoordinate2D)coordinate1 coordinate2:(CLLocationCoordinate2D)coordinate2 onCompletion:(void(^)(NSArray <Taxi*> *results, NSError *error))completion;
@end
