//
//  MyTaxiListWireframe.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MyTaxiListProtocols.h"

@class MyTaxiListViewController;

@interface MyTaxiListWireframe : NSObject<MyTaxiListWireframeProtocol>

@property(nonatomic, weak) MyTaxiListViewController *viewController;

+ (void)presentScreenFromViewController:(UIViewController *)viewController;
+ (UIViewController *)setupViewController;

@end
