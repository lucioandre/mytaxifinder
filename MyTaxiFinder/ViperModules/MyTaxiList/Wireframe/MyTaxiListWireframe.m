//
//  MyTaxiListWireframe.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "MyTaxiListWireframe.h"
#import "MyTaxiListViewController.h"
#import "MyTaxiListInteractor.h"
#import "MyTaxiListPresenter.h"
#import "TaxiSearchAPIClient.h"

@implementation MyTaxiListWireframe
 
#pragma mark - Public
    
+ (void)presentScreenFromViewController:(UIViewController *)viewController {
    UIViewController *presentingViewController = [self setupViewController];
    [viewController.navigationController pushViewController:presentingViewController animated:YES];
}

+ (UIViewController *)setupViewController {
    MyTaxiListPresenter *presenter = [MyTaxiListPresenter new];
    MyTaxiListWireframe *wireframe = [MyTaxiListWireframe new];
    MyTaxiListInteractor *interactor = [[MyTaxiListInteractor alloc] initWithTaxiSearchAPIClient:[TaxiSearchAPIClient new]];
    
    MyTaxiListViewController *viewController = [[MyTaxiListViewController alloc] initWithNibName:NSStringFromClass(MyTaxiListViewController.class) bundle:nil];
    viewController.presenter = presenter;
    
    presenter.wireframe = wireframe;
    presenter.view = viewController;
    presenter.interactor = interactor;
    
    wireframe.viewController = viewController;
    
    return viewController;
}
    
@end
