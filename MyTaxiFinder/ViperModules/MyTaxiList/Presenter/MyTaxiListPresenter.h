//
//  MyTaxiListPresenter.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTaxiListProtocols.h"

@interface MyTaxiListPresenter : NSObject<MyTaxiListPresenterProtocol>

@property(nonatomic, weak) id<MyTaxiListViewProtocol> view;
@property(nonatomic) id<MyTaxiListWireframeProtocol> wireframe;
@property(nonatomic) id<MyTaxiListInteractorProtocol> interactor;

@end
