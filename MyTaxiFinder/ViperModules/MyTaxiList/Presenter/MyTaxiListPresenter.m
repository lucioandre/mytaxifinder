//
//  MyTaxiListPresenter.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "MyTaxiListPresenter.h"

@interface MyTaxiListPresenter ()

@end

@implementation MyTaxiListPresenter

- (void)viewDidLoadEvent {
    [self loadData];
}

- (void)reloadDataEvent {
    [self loadData];
}

- (void)loadData {
    CLLocationCoordinate2D coordinate1 = CLLocationCoordinate2DMake(53.694865, 9.757589);
    CLLocationCoordinate2D coordinate2 = CLLocationCoordinate2DMake(53.394655, 10.099891);

    [self.view showProgressIndicator];
    [self.interactor searchTaxisBetweenCoordinate1:coordinate1 coordinate2:coordinate2 onCompletion:^(NSArray<Taxi *> *results, NSError *error) {
        [self.view hideProgressIndicator];
        //TODO: Handle Error appropriately
        if (!error) {
            [self.view updateTaxiSearchResults:results];
        }
    }];

}

@end
