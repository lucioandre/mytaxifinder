//
//  MyTaxiListTableViewCell.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//  Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "MyTaxiListTableViewCell.h"

@interface MyTaxiListTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end

@implementation MyTaxiListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 4;
    self.containerView.layer.masksToBounds = YES;
}

- (void)setActiveState:(BOOL)activeState {
    self.stateImageView.image = [UIImage imageNamed:activeState ? @"taxi-green" : @"taxi-red"];
    self.stateLabel.text = activeState ? @"ACTIVE" : @"INACTIVE";
    self.stateLabel.textColor = activeState ? [UIColor greenColor] : [UIColor redColor];
}

@end
