//
//  MyTaxiListViewController.h
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTaxiListProtocols.h"

@interface MyTaxiListViewController : UIViewController<MyTaxiListViewProtocol>

@property(nonatomic) id<MyTaxiListPresenterProtocol> presenter;

@end
