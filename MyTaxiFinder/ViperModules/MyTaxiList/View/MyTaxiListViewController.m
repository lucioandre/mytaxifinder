//
//  MyTaxiListViewController.m
//  MyTaxiFinder
//
//  Created by Avenue Brazil on 14/10/17.
//Copyright © 2017 Lucio Fonseca. All rights reserved.
//

#import "MyTaxiListViewController.h"
#import "Taxi.h"
#import "MyTaxiListTableViewCell.h"
#import "MBProgressHUD.h"

@interface MyTaxiListViewController () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray <Taxi *>* taxiResults;
@end

@implementation MyTaxiListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 83;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadData)];
    self.navigationItem.rightBarButtonItem = reloadButton;

    NSString *cellNibName = NSStringFromClass(MyTaxiListTableViewCell.class);
    [self.tableView registerNib:[UINib nibWithNibName:cellNibName bundle:nil] forCellReuseIdentifier:cellNibName];

    self.navigationController.navigationBar.prefersLargeTitles = YES;
    self.title = @"Taxi List";

    [self.presenter viewDidLoadEvent];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Protocol

- (void)showProgressIndicator {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideProgressIndicator {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)updateTaxiSearchResults:(NSArray<Taxi *> *)results {
    self.taxiResults = results;
    [self.tableView reloadData];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.taxiResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyTaxiListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MyTaxiListTableViewCell.class) forIndexPath:indexPath];
    if (self.taxiResults.count > indexPath.row) {
        Taxi *taxi = self.taxiResults[indexPath.row];
        cell.titleLabel.text = taxi.type;
        cell.coordinatesLabel.text = [NSString stringWithFormat:@"Latitude: %f, Longitude: %f", taxi.latitude.doubleValue, taxi.longitude.doubleValue];
        cell.activeState = [taxi.state.lowercaseString isEqualToString:@"active"];
    }
    return cell;
}

#pragma mark - Reload Data

- (void)reloadData {
    [self.presenter reloadDataEvent];
}

@end
